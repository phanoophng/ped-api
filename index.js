const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const routes = require('./routes/index')

const app = express()

const PORT = 4000

// middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

// mount api route
app.use('/', routes)

app.listen(PORT, () => {
  console.log('App start on port ' + PORT)
})