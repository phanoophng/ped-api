# #!/bin/bash
docker build -t asia.gcr.io/newcar-production/interview-api .
docker push  asia.gcr.io/newcar-production/interview-api

gcloud compute ssh interview-api << EOF
docker pull asia.gcr.io/newcar-production/interview-api
docker stop api || true
docker rm api || true
docker rmi asia.gcr.io/newcar-production/interview-api:current || true
docker tag asia.gcr.io/newcar-production/interview-api:latest asia.gcr.io/newcar-production/interview-api:current
docker run -d --restart always --name api -p 80:80 asia.gcr.io/newcar-production/interview-api:current
EOF