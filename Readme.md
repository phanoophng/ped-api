## Requirements

 - [Node v7.6+](https://nodejs.org/en/download/current/) or [Docker](https://www.docker.com/)
 - [Yarn](https://yarnpkg.com/en/docs/install)

## Getting Started

```bash
git clone https://bitbucket.org/phanoophng/ped-api.git

cd ped-api
```

Install dependencies:

```bash
yarn
```

Running:

```bash
yarn start
```

## Documentation

```bash
 query user

 GET /users


  ให้ส่ง parameter ไปทาง query params

  - 'search'

  /users?search=chai // หา ชื่อ นามสกุล หรือ email จาก parameter search


  - 'age' สำหรับค้นหาอายุแบบระบุ

  /users?age=10


  - 'minAge', 'maxAge' // สำหรับหาช่วงอายุ

  /users?minAge=10&maxAge=20 // หาจากอายุ 10 - 20 ปี

  /users?minAge=10&maxAge=20&minAge=50&maxAge=70 // หาจาก 2 ช่วงอายุ โดยลำดับการวาง parameter ต้องวางคู่กัน


  - 'start', 'limit'

  /user?start=20&limit=50 // เริ่มหา users 50 คนโดยเริ่มจากคนที่ 20


  - 'sort'
  /users?sort=id // สามารถเรียงลำดับจาก field ต่อไปนี้ 'id', 'first_name', 'last_name', 'emai', 'age', 'gender'

```