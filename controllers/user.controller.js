const path = require('path')
const sqlite3 = require('sqlite3').verbose()
const sqlite = require('sqlite')

const dbPath = path.resolve(__dirname, '../db/user.db')
const dbPromise = sqlite.open(dbPath, { Promise })

const renderSearchText = search => {
  if (typeof search === 'string') {
    return `'${search}'`
  }
  else if (typeof search === 'object') {
    return search.map(d => `'${d}'`).join(',');
  }
}

const list = async (req, res, next) => {
  try {
    const { search, start, limit, itemPerPag, email, id, first_name, last_name, minAge, maxAge, gender, age, sort } = req.query
    const queryLimit = start || limit ? `LIMIT ${start || 0},${limit || 100}` : ''
    const queryId = id  ? `id = ${id}` : ''
    let queryAge = ``
    const queryEmail = email  ? `email = '${email}'` : ``
    const queryFirstName = first_name  ? `first_name = '${first_name}'` : ``
    const queryLastName = last_name  ? `last_name = '${last_name}'` : ``
    const queryGender = gender ? `gender =  '${gender}'` : ''
    let queryAgeRange = ``

    if (age && typeof age === 'object') {
      let ageArr = []
      for (let a of age ) {
        const query = `age =  ${a}`
        ageArr = [...ageArr, query]
      }
      queryAge = `(${ageArr.join(' OR ')})`
    } else if (age && typeof age === 'string') {
      queryAge = `age = ${age}`
    }

    if (minAge && typeof minAge === 'object') {
      let ageRangeQueryString = []
      for (let i in minAge) {
        const from = minAge[i]
        const to = maxAge[i]
        const query = `(age BETWEEN ${from} AND ${to})`
        ageRangeQueryString = [ ...ageRangeQueryString, query]
      }
      queryAgeRange = ageRangeQueryString.join(' OR ')
    } else if (minAge && typeof minAge === 'string') {
      queryAgeRange = `(age BETWEEN ${minAge || 0} AND ${maxAge || 999})`
    }
    let queryArr = [queryEmail, queryFirstName, queryLastName, queryGender, queryId, queryAge, queryAgeRange]
    let query = ''
    if (search) {
      queryArr = [queryGender, queryAge, queryAgeRange]
      queryArr = await queryArr.map(d => {
        if (d !== ``) return d
      })
      queryArr = await queryArr.filter(d => d !== undefined)
      const searchQuery = renderSearchText(search)
      query = `
      WHERE (
        email IN (${searchQuery}) OR
        first_name IN (${searchQuery}) OR
        last_name IN (${searchQuery}) OR
        id IN (${searchQuery})
      ) ${queryArr.length > 0 ? `AND (${queryArr.join(' OR ')})` : ''}
      `
    } else {
      queryArr = await queryArr.map(d => {
        if (d !== ``) return d
      })
      queryArr = await queryArr.filter(d => d !== undefined)
      let conditions = queryArr.length > 0 ? `WHERE ${queryArr.join(' OR ')}` : ''
      query = `${conditions}`
    }

    const db = await dbPromise
    const total = await db.get(`SELECT COUNT(*) FROM 'users' ${query}`)
    const users = await db.all(`SELECT * FROM 'users' ${query} ORDER BY ${sort || 'id'} ${queryLimit}`)
    const totalUser = total[`COUNT(*)`]
    res.status(200).send({total: totalUser, users})
  } catch (err) {
    throw err
  }
}

module.exports = {
  list
}